document.addEventListener("DOMContentLoaded", function() {
    const array = [1, 2, 3, 4, 4, 5, 6, 7, 8, 9, 10, 11, 12];

    document.getElementsByClassName('result')[0].innerText = getDuplicate(array);

    function getDuplicate(array) {
        const middle = Math.floor((0 + (array.length - 0) / 2));
        if (array.length === 1) {
            return array[0];
        } else if (array[middle] === middle + array[0] * 1) {
            array.splice(0, middle);
            return getDuplicate(array);
        } else {
            array.splice(middle, array.length);
            return getDuplicate(array);
        };

    }
});